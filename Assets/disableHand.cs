﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableHand : Photon.MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		if (this.photonView.isMine) {
			MeshRenderer[] meshes = GetComponentsInChildren<MeshRenderer> ();	
			for (int i = 0; i < meshes.Length; i++) {
				if(meshes[i].gameObject.name == "hand"){
					meshes [i].gameObject.SetActive (false);
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
