﻿using System.Collections;
using UnityEngine;
using AssetBundles;

public class loader : MonoBehaviour {

	// Use this for initialization
	public static string url = "http://schonba2.web.engr.illinois.edu/thisbetterwork";

	public void loadObject(){
		StartCoroutine (loadWWWCaching (url));
	}


	IEnumerator loadWWWCaching (string url){
//		while (!Caching.ready)
//			yield return null;
//		using (WWW www = WWW.LoadFromCacheOrDownload (url, 1)) {
//			yield return www;
//			if (www.error == null) {
//				AssetBundle assetBundle = www.assetBundle;
//				// load asset asynchronously
//				for(int i=0;i<assetBundle.GetAllAssetNames().Length;i++){
//					print(assetBundle.GetAllAssetNames()[i]);
//				}
//				AssetBundleRequest request = assetBundle.LoadAssetAsync ("MMSEV", typeof(GameObject));
//				// Wait for request to complete
//				yield return request;
//				GameObject prefab = request.asset as GameObject;
//
//				if (prefab != null) {
//					GameObject.Instantiate (prefab);
//				}
////				assetBundle.Unload (true);
//			} else {
//				Debug.Log ("WWW error: " + www.error);
//			}
//		}

		WWW www = new WWW (url);
		yield return www;
		if (www.error == null) {
			AssetBundle assetBundle = www.assetBundle;
			AssetBundleRequest request = assetBundle.LoadAssetAsync ("MMSEV", typeof(GameObject));
			// Wait for request to complete
			yield return request;
			GameObject prefab = request.asset as GameObject;

			if (prefab != null) {
				GameObject hosk = Instantiate (prefab, new Vector3(0f,0f, 0f), Quaternion.identity);
				Renderer deez = hosk.AddComponent<MeshRenderer> () as Renderer;
				float center = deez.bounds.center.y;
				float min = deez.bounds.min.y;

				print (center + "," + min);
				hosk.transform.position = new Vector3 (2f, -1 + center - min, 0f);



			}
		} else {
			Debug.Log ("WWW error: " + www.error);
		}
	}
}
