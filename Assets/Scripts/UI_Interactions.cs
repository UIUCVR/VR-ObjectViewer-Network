﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Interactions : MonoBehaviour {
	public void setRed()
	{
		GameObject redCube = GameObject.Find("Cube");
		redCube.GetComponent<Renderer>().material.color = Color.red;

		GameObject redSphere = GameObject.Find("Sphere");
		redSphere.GetComponent<Renderer>().material.color = Color.red;
	}

	public void setBlue()
	{
		GameObject blueCube = GameObject.Find("Cube");
		blueCube.GetComponent<Renderer>().material.color = Color.blue;

		GameObject blueSphere = GameObject.Find("Sphere");
		blueSphere.GetComponent<Renderer>().material.color = Color.blue;
	}
		
}
