﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class loadAssetBundle : MonoBehaviour {

	// Use this for initialization
	public string url = "http://schonba2.web.engr.illinois.edu/thisbetterwork";
	public AssetBundle assetBundle;
	void Start () {
		StartCoroutine (loadWWWCaching (url));

	}
	
	// Update is called once per frame
	IEnumerator loadWWWCaching (string url){
		WWW www = new WWW (url);
		yield return www;
		if (www.error == null) {
			assetBundle = www.assetBundle;

			//LOAD BUTTONS TO CHOOSE WHAT TO LOAD
			GameObject real = (GameObject) Resources.Load("Button");
			string[] names = assetBundle.GetAllAssetNames ();
			for (int i = 0; i < names.Length; i++) {
				GameObject child = Instantiate (real, real.transform.position, real.transform.rotation, GameObject.Find ("Content").transform);
				child.transform.rotation = new Quaternion (0, 0, 0, 0);
				child.GetComponentInChildren<Text> ().text = names [i].Substring(7);
			}

		} else {
			Debug.Log ("WWW error: " + www.error);
		}
	}
		

	//FUNCTION CALLED BY OPENCAD TO OPEN OBJECT
	public void load(string nam){
		
		PhotonView photonView = this.GetComponent<PhotonView>();
		photonView.RPC("runIEnumerator", PhotonTargets.AllBuffered, nam);
	}


	public void transformPos(Vector3 amt){
		PhotonView photonView = this.GetComponent<PhotonView>();
		photonView.RPC ("RPCTransformPos", PhotonTargets.AllBuffered, amt);
	}

	public void transformRot(float amt){
		PhotonView photonView = this.GetComponent<PhotonView>();
		photonView.RPC ("RPCTransformRot", PhotonTargets.AllBuffered, amt);
	}

	public void transformScale(float amt){
		PhotonView photonView = this.GetComponent<PhotonView>();
		photonView.RPC ("RPCScale", PhotonTargets.AllBuffered, amt);
	}


	[PunRPC]
	public void RPCTransformPos(Vector3 amt){
		GameObject CAD = GameObject.FindGameObjectWithTag ("CAD");
		Vector3 curr = CAD.transform.position;
		curr += amt;
		CAD.transform.position = curr;
	}

	[PunRPC]
	public void RPCTransformRot(float amt){
		GameObject CAD = GameObject.FindGameObjectWithTag ("CAD");
		Quaternion curr = CAD.transform.rotation;
		curr.y+=amt;
		CAD.transform.rotation = curr;
	}

	[PunRPC]
	public void RPCScale(float amt){
		GameObject CAD = GameObject.FindGameObjectWithTag ("CAD");
		Vector3 curr = CAD.transform.localScale;
		curr+= new Vector3(amt,amt,amt);
		CAD.transform.localScale = curr;
	}

	//RPC CALL TO START COROUTINE
	[PunRPC]
	public void runIEnumerator(string nam){
		StartCoroutine (loadObj (nam, assetBundle));
	}

	//ACTUAL LOAD OBJECT FUNCTION
	IEnumerator loadObj(string nam, AssetBundle assetBundle){
		
		AssetBundleRequest request = assetBundle.LoadAssetAsync (nam, typeof(GameObject));
		// Wait for request to complete
		yield return request;


		GameObject prefab = request.asset as GameObject;

		if (prefab != null) {
			
			DestroyObject (GameObject.FindGameObjectWithTag ("CAD"));
			GameObject obj = (GameObject) Instantiate (prefab,new Vector3(0f,0f,0f), Quaternion.identity);
			obj.tag = "CAD";
			obj.AddComponent<PhotonView> ();

			//NORMALIZE ONTO THE PLATFORM
			if (obj.GetComponent<MeshRenderer> () != null) {
				Bounds bound2 = obj.GetComponent<MeshRenderer> ().bounds;
				obj.transform.position = new Vector3 (0.93f, bound2.extents.y + 1.5f, 0.82f);
			} else {
				obj.AddComponent<MeshRenderer> ();
				Bounds bound = obj.GetComponent<MeshRenderer> ().bounds;
				Transform secondChild = obj.transform.GetChild (0);
				foreach (Transform child in secondChild) {
					if (child.childCount > 0) {
						foreach (Transform otherchild in child) {
							bound.Encapsulate (otherchild.GetComponent<MeshRenderer> ().bounds);
						}
					} 
					else {
						bound.Encapsulate (child.GetComponent<MeshRenderer> ().bounds);
					}
				}
				obj.transform.position = new Vector3 (0.93f, bound.extents.y+1.5f, 0.82f);
			}
		}
//				assetBundle.Unload (true);
	}
}
