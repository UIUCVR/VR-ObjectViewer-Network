﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

public class SphinxTest : MonoBehaviour {

	string str;


	// Use this for initialization
	void Start () {
		UnitySphinx.Init ();
		UnitySphinx.Run ();
	}

	void Update()
	{
		str = UnitySphinx.DequeueString ();

		if (UnitySphinx.GetSearchModel() == "kws")
		{
			//print ("listening for keyword");
			if (str != "") {
				UnitySphinx.SetSearchModel (UnitySphinx.SearchModel.jsgf);
				//print (str);
			}
		}
		else if (UnitySphinx.GetSearchModel() == "jsgf")
		{
			//print ("listening for order");
			if (str != "") 
			{
				char[] delimChars = { ' ' };
				string[] cmd = str.Split (delimChars);

				if (cmd [0] != "everything") {
					GameObject obj = interpretObj (cmd [0]);
					Color col = interpretColor (cmd [1]);

					if (obj != null) {
						obj.GetComponent<MeshRenderer> ().material.color = col;
					}
				} else {
					GameObject obj = GameObject.FindGameObjectWithTag ("CAD");
					MeshRenderer[] deez = obj.GetComponentsInChildren<MeshRenderer> ();
					foreach(MeshRenderer mes in deez){
						mes.material.color = interpretColor (cmd [1]);
					}
				}

				UnitySphinx.SetSearchModel (UnitySphinx.SearchModel.kws);

			}
		}
	}

	GameObject interpretObj(string obj)
	{
		//print (obj);
		obj = obj.ToUpper ();
		GameObject gObj = GameObject.Find (obj);
		return gObj;

	}

	Color interpretColor(string col){
		//print (col);
		switch (col) {
		case "red":
			return Color.red;
		case "yellow":
			return Color.yellow;
		case "green":
			return Color.green;
		case "grey":
			return Color.grey;
		case "white":
			return Color.white;
		case "black":
			return Color.black;
		}
		return Color.clear;
		
	}
		
}